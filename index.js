'use strict';

const http = require('http');

const Async = require('@scola/async');
const Deep = require('@scola/deep');
const DI = require('@scola/di');
const Message = require('@scola/transport-message');

const Client = require('./lib/client');
const Connection = require('./lib/connection');

class Module extends DI.Module {
  configure() {
    this.inject(Client).with(
      this.provider(Connection),
      this.provider(Message)
    );

    this.inject(Connection).with(
      this.array([]),
      this.provider(Async.Serial),
      this.provider(Message),
      this.instance(Deep.Map),
      this.value(http)
    );
  }
}

module.exports = {
  Client,
  Connection,
  Module
};

'use strict';

const Abstract = require('@scola/transport-http-common');
const Error = require('@scola/error');

class Connection extends Abstract.Connection {
  constructor(filters, serial, message, options, http) {
    super(filters, serial, message);

    this.options = options;
    this.http = http;
  }

  open(options) {
    this.emit('debug', this, 'open', options);
    this.options.assign(options);

    return this;
  }

  close() {
    this.request.end();
    this.handleClose();

    return this;
  }

  canSend() {
    return !this.response;
  }

  bindListeners() {
    this.bindListener('data', this.response, this.handleReceiveData);
    this.bindListener('end', this.response, this.handleReceiveEnd);
    this.bindListener('close', this.request, this.handleClose);
  }

  unbindListeners() {
    this.unbindListener('data', this.response, this.handleReceiveData);
    this.unbindListener('end', this.response, this.handleReceiveEnd);
    this.unbindListener('close', this.request, this.handleClose);
  }

  handleClose() {
    this.unbindListeners();
    this.emit('close', this);
  }

  handleSend(message, close) {
    this.emit('debug', this, 'handleSend', message, close);

    const options = this.options
      .clone()
      .assign({
        headers: message.getHeaders()
      });

    this.request = this.http.request(options.toJSON(), (response) => {
      this.response = response;
      this.bindListeners();
    });

    this.request.write(message.getBody());

    if (close !== false) {
      this.request.end();
    }
  }

  handleReceiveEnd() {
    this.emit('debug', this, 'handleReceiveEnd', this.data);

    if (Error.isError(this.data)) {
      this.handleError(Error.fromString(this.data));
    } else {
      super.handleReceiveEnd();
    }

    this.response = null;
  }
}

module.exports = Connection;

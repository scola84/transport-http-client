'use strict';

const EventHandler = require('@scola/events');

class Client extends EventHandler {
  constructor(connection, message) {
    super();

    this.connectionProvider = connection;
    this.messageProvider = message;
  }

  connect(options) {
    this.emit('debug', this, 'connect', options);

    const connection = this.connectionProvider
      .get()
      .open(options);

    this.emit('connection', connection);
  }

  createMessage() {
    this.emit('debug', this, 'createMessage');
    return this.messageProvider.get();
  }
}

module.exports = Client;
